<?php
use Sda\TrystarAPI\Cycler\Cycler;
use Sda\TrystarAPI\Db\DbConnection;
use Sda\TrystarAPI\Config\Config;
use Sda\TrystarAPI\Request\Request;

require_once(__DIR__ . '/../vendor/autoload.php');

$db = new DbConnection(
    Config::$connectionParams
);
$request=new Request();

$app =new Cycler($db,$request);
$app->run();