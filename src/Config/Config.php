<?php

namespace Sda\TrystarAPI\Config;

class Config
{
    const DEBUG = true;

    const ABSOLUTE_TEMPLATE_DIR = __DIR__ . '/../View/';
    const ABSOLUTE_CACHE_TEMPLATE_DIR = __DIR__ . '/../../cache';

    const KEY_FROM_HEADER='12345678901234567890123456789012';
    const API_URL = "http://trystar.local/api/lights";
    public static $connectionParams = array(
        'dbname' => 'trystarapi',
        'user' => 'root',
        'password' => '',
        'host' => 'localhost',
        'driver' => 'pdo_mysql',
        'charset' => 'utf8'
    );
}