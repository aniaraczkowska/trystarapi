<?php
/**
 * Created by PhpStorm.
 * User: RENT
 * Date: 2016-08-06
 * Time: 09:20
 */

namespace Sda\TrystarAPI\Controller;


use Sda\TrystarAPI\Crossroad\CrossroadRepository;
use Sda\TrystarAPI\Cycle\CycleRepository;
use Sda\TrystarAPI\Db\DbConnection;
use Sda\TrystarAPI\Request\Request;
use Sda\TrystarAPI\Response\Response;
use Sda\TrystarAPI\Config\Routing;
use Twig_Environment;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;


/**
 * Class Controller
 * @package Sda\TrystarAPI\Controller
 */
class Controller
{


    /**
     * @var DbConnection
     */
    private $connection;
    private $response;
    private $request;
    private $templates;

    public function __construct(Twig_Environment $templates, Request $request, Response $response, DbConnection $connection)
    {
        $this->templates = $templates;
        $this->request = $request;
        $this->response = $response;


        $this->crossroads = (new CrossroadRepository($connection))->getCrossroads();
        $this->cycles=(new CycleRepository($connection))->getCycles();

       // $this->connection = $connection;
    }

    public function run()
    {
        $action = $this->request->getParamFromGet('action', Routing::PANEL);

        switch ($action) {
            case Routing::PANEL:
                $this->openAction();
                break;
            default:
                $this->error404Action();
                break;
        }
    }

    private function openAction()
    {
        $params['crossroads']=$this->crossroads;
        $params['cycles']=$this->cycles;

        $this->renderTemplate('panel.tmpl.html', $params);
    }

    private function renderTemplate($template, array $params)
    {
        try {
            echo $this->templates->render($template, $params);
        } catch (Twig_Error_Loader $e) {
            echo $e->getMessage();
        } catch (Twig_Error_Syntax $e) {
            echo $e->getMessage();
        } catch (Twig_Error_Runtime $e) {
            echo $e->getMessage();
        }
    }

    private function error404Action()
    {
        $this->renderTemplate('error_404.tmpl.html', []);
    }



}