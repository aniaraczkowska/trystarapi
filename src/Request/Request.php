<?php
namespace Sda\TrystarAPI\Request;

use Sda\TrystarAPI\Config\Config;

class Request{
    const  HTTP_METHOD_GET = 'GET';
    const HTTP_METHOD_POST = 'POST';

    /**
     * @param $param
     * @param string $default
     * @return string
     */
    public function getParamFromGet($param, $default = '')
    {
        if (true === array_key_exists($param, $_GET)) {
            return $_GET[$param];
        }

        return $default;
    }

    /**
     *
     * @param String $param
     * @param string $default
     * @return string
     */
    public function getParamsFromPost($param, $default = '')
    {
        if (true === array_key_exists($param, $_POST)) {
            return $_POST[$param];
        }

        return $default;
    }

    /**
     * @param $jsonData
     * @param $authKey
     * @throws ApiUpdateException
     */
    public function sendApiRequest($jsonData,$authKey)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => Config::API_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 1,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonData,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
                "X-CROSSROAD-AUTH:".$authKey
            ),
        ));


        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new ApiUpdateException();
        }
    }
}