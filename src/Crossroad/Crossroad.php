<?php

namespace Sda\TrystarAPI\Crossroad;


class Crossroad
{

    private $id;
    private $name;
    private $auth_key;
    private $host;
    private $active;


    /**
     * Crossroad constructor.
     * @param $id
     * @param $name
     * @param $
     */
    public function __construct($id, $name, $auth_key, $host, $active)
{
    $this->id = $id;
    $this->name = $name;
    $this->auth_key = $auth_key;
    $this->host = $host;
    $this->active = $active;
}

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }
}