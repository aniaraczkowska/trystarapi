<?php

namespace Sda\TrystarAPI\Crossroad;


use Sda\TrystarAPI\TypedCollection;

class CrossroadCollection extends TypedCollection
{
    public function __construct()
    {
        $this->setItemType(Crossroad::class);
    }
}