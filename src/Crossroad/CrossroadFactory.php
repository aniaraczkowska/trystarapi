<?php


namespace Sda\TrystarAPI\Crossroad;


class CrossroadFactory
{

    public static function makeFromArray(array $param)
    {

        return new Crossroad($param['id'],$param['name'],$param['auth_key'],$param['host'],$param['active']);
    }

    public static function makeCrossroadName(array $param){
      $crossroad=new   Crossroad($param['id'],$param['name'],$param['auth_key'],$param['host'],$param['active']);
        $result=[$crossroad->getName(),
        $crossroad->getId()];
        return $result;
}
}