<?php


namespace Sda\TrystarAPI\Crossroad;


use Sda\TrystarAPI\Db\DbConnection;

class CrossroadRepository
{
    /**
     * @var DbConnection
     */
    private $dbConnection;

    /**
     * LightRepository constructor.
     * @param DbConnection $dbConnection
     * @internal param DbConnection $connection
     */
    public function __construct(DbConnection $dbConnection)
    {

        $this->dbConnection = $dbConnection;
    }

    /**
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function getQueryBuilder()
    {
        return $this->dbConnection->getConnection()->createQueryBuilder();
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getCrossroads()
    {
        $query = $this->getQueryBuilder();
        $sth = $query
            ->select('*')
            ->from('crossroads')
            ->execute();
       $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        $collection = new CrossroadCollection();



        foreach ($data as $crossroad) {

            $collection->add(CrossroadFactory::makeFromArray($crossroad));
        }


        return $collection;

        }
}