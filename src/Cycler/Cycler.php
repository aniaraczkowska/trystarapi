<?php

namespace Sda\TrystarAPI\Cycler;


use Sda\TrystarAPI\Db\DbConnection;
use Sda\TrystarAPI\Request\Request;


/**
 * Class Cycler
 * @package Sda\Mastercross\Cycler
 */
class Cycler
{


    /**
     * @var string
     */
    private $apiKey;
    /**
     * @var int
     */
    private $crossroadId;
    /**
     * @var
     */
    private $cycleRepository;
    /**
     * @var Request
     */
    private $request;

    public function __construct(DbConnection $connection, Request $request)
    {
        $this->request = $request;
        $this->cycleRepository = new CyclerRepository($connection);
    }

    public function run()
    {
        $crossroads = $this->cycleRepository->getCrossroads();


        foreach ($crossroads as $crossroad) {

            $this->apiKey = $crossroad['auth_key'];

            $this->crossroadId = $crossroad['id'];

            $cycle = $this->cycleRepository->getActiveCycleForCrossroad($this->crossroadId);

            $currentState = $this->cycleRepository->getCrossroadStates($this->crossroadId);

            if (false === $currentState) {
                $phase = $this->cycleRepository->getFirstPhaseFromCycle($cycle['id']);
            } else {
                $phase = $this->cycleRepository->getPhaseById($currentState['phases_id']);

                $interval = (int)$phase['interval'] / 1000;
                $actualStartTime = strtotime($currentState['created_at']);
                $now = time();


                if (($now - $actualStartTime) > $interval) {
                    try {
                        $phase = $this->cycleRepository->getNextPhase($cycle['id'], $phase['priority']);
                    } catch (PhaseNotFoundException $e) {
                        $phase = $this->cycleRepository->getFirstPhaseFromCycle($cycle['id']);
                    }
                } else {
                    continue;
                }
            }

            $this->activatePhase($phase, $this->apiKey);
        }
    }

    /**
     * @param array $phase
     * @param $authKey
     */
    private function activatePhase(array $phase, $authKey)
    {
        $lights = $this->cycleRepository->getPhaseLights($phase['id']);

        try {

            $this->request->sendApiRequest(json_encode($lights), $authKey);
            $this->cycleRepository->updateCrurrentCrossroadState($phase['id'], $this->crossroadId);
        } catch (ApiUpdateException $e) {
            $this->logError($e);
        }
    }

    /**
     * @param \Exception $e
     */
    private function logError($e)
    {
        //TODO - dorobic logowanie
        echo $e->getMessage();
    }


}