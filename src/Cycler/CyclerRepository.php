<?php
namespace Sda\TrystarAPI\Cycler;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Doctrine\DBAL\Query\QueryBuilder;
use Sda\TrystarAPI\Db\DbConnection;

/**
 * Class CyclerRepository
 * @package Sda\TrystarAPI\Cycler
 */
class CyclerRepository{

    /**
     * @var DbConnection
     */
    private $dbConnection;

    /**
     * Cycler constructor.
     * @param DbConnection $dbConnection
     */
    public function __construct(DbConnection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    /**
     * @return mixed
     */
    public function getCrossroadStates($crossroadId)
    {
        $query = $this->getQueryBuilder();
        if(empty($crossroadId)) {
            throw new InvalidArgumentException('nie ma ...');
        }
        $sth = $query
            ->select('*')
            ->from('current_crossroads_state')
            ->where('crossroads_id = :crossroadId')
            ->setParameter('crossroadId', $crossroadId)
            ->execute();

        $data = $sth->fetch(\PDO::FETCH_ASSOC);

        return $data;
    }



    /**
     * @return array
     */
    public function getCrossroads(){
        $query = $this->getQueryBuilder();
        $sth = $query
            ->select('*')
            ->from('crossroads')
            ->execute();

        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }

    /**
     * @param $crossroadId
     * @return array
     */
    public function getActiveCycleForCrossroad($crossroadId)
    {
        $query = $this->getQueryBuilder();
        $sth = $query
            ->select('*')
            ->from('cycles', 'c')
            ->where('c.crossroads_id = :crossroadId')
            ->setParameter('crossroadId', $crossroadId)
            ->andWhere("c.active = 'yes'")
            ->execute();

        $data = $sth->fetch(\PDO::FETCH_ASSOC);

        return $data;
    }

    /**
     * @return QueryBuilder
     */
    private function getQueryBuilder()
    {
        return $this->dbConnection->getConnection()->createQueryBuilder();
    }

    /**
     * @param $cycleId
     * @return array
     */
    public function getFirstPhaseFromCycle($cycleId)
    {
        $query = $this->getQueryBuilder();
        $sth = $query
            ->select('*')
            ->from('phases')
            ->where('cycles_id = :cycleId')
            ->setParameter('cycleId', $cycleId)
            ->andWhere('priority = 0')
            ->execute();

        $data = $sth->fetch(\PDO::FETCH_ASSOC);

        return $data;
    }

    /**
     * @param int $phaseId
     */
    public function getPhaseById($phaseId)
    {
        $query = $this->getQueryBuilder();

        $sth = $query
            ->select('*')
            ->from('phases')
            ->where('id = :phaseId')
            ->setParameter('phaseId', $phaseId)
            ->execute();

        $data = $sth->fetch(\PDO::FETCH_ASSOC);

        return $data;

    }

    /**
     * @param int $cycleId
     * @param int $priority
     * @return array
     * @throws PhaseNotFoundException
     */
    public function getNextPhase($cycleId, $priority)
    {
        $query = $this->getQueryBuilder();

        $sth = $query
            ->select('*')
            ->from('phases')
            ->where('cycles_id = :cycleId')
            ->setParameter('cycleId', $cycleId)
            ->andWhere('priority = :prio')
            ->setParameter('prio', ($priority + 1))
            ->execute();

        $data = $sth->fetch(\PDO::FETCH_ASSOC);

        if (false === $data){
            throw new PhaseNotFoundException();
        }

        return $data;
    }




    /**
     * @param $phaseId
     */
    public function getPhaseLights($phaseId)
    {
        $query = $this->getQueryBuilder();

        $sth = $query
            ->select('cl.external_id AS id, lp.state')
            ->from('crossroad_lights', 'cl')
            ->join('cl', 'crossroads', 'c', 'c.id = cl.crossroads_id')
            ->join('c', 'cycles', 'cc', 'c.id = cc.crossroads_id')
            ->join('cc', 'phases', 'p', 'cc.id = p.cycles_id')
            ->join('cl', 'light_phases', 'lp', 'cl.id = lp.crossroad_lights_id')
            ->where('lp.phases_id = :phaseId')
            ->setParameter('phaseId', $phaseId)
            ->groupBy('cl.id')
            ->execute();


        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        return $data;
    }

    public function updateCrurrentCrossroadState($phaseId,$crossroadId)
    {
        $sql = '
            INSERT INTO `current_crossroads_state`
            ( `crossroads_id`, `phases_id`, `created_at`)
            VALUES 
            (
                :crossroadsid,
                :phasesid,
                CURRENT_TIMESTAMP 
            )
            ON DUPLICATE KEY UPDATE
              `crossroads_id` = VALUES(`crossroads_id`),
              `phases_id` = VALUES(`phases_id`),
              `created_at` = CURRENT_TIMESTAMP
        ';

        $params = [
            'crossroadsid' => $crossroadId,
            'phasesid' => $phaseId
        ];

        $this->dbConnection->getConnection()->executeUpdate($sql, $params);

    }

}