<?php

use Sda\TrystarAPI\Config\Config;
use Sda\TrystarAPI\Controller\Controller;
use Sda\TrystarAPI\Db\DbConnection;
use Sda\TrystarAPI\Request\Request;
use Sda\TrystarAPI\Response\Response;

require_once(__DIR__ . '/../vendor/autoload.php');

$twig = new Twig_Environment(
    new Twig_Loader_Filesystem(Config::ABSOLUTE_TEMPLATE_DIR),
    array(
        'cache' => (false === Config::DEBUG) ? Config::ABSOLUTE_CACHE_TEMPLATE_DIR : false
    )
);


$api = new Controller(
    $twig,
    new Request(),
    new Response(),
    new DbConnection(Config::$connectionParams)
);

$api->run();