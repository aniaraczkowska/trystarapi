<?php

namespace Sda\TrystarAPI\Cycle;

class Cycle
{
    private $id;
    private $crossroads_id;
    private $name;
    private $active;

    public function __construct($id, $crossroads_id, $name, $active)
{
    $this->id = $id;
    $this->crossroads_id = $crossroads_id;
    $this->name = $name;
    $this->active = $active;
}

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCrossroadsId()
    {
        return $this->crossroads_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

}