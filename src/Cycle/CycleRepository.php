<?php
/**
 * Created by PhpStorm.
 * User: RENT
 * Date: 2016-08-06
 * Time: 14:53
 */

namespace Sda\TrystarAPI\Cycle;


use Sda\TrystarAPI\Db\DbConnection;

class CycleRepository
{
    /**
     * @var DbConnection
     */
    private $dbConnection;

    /**
     * LightRepository constructor.
     * @param DbConnection $dbConnection
     * @internal param DbConnection $connection
     */
    public function __construct(DbConnection $dbConnection)
    {

        $this->dbConnection = $dbConnection;
    }

    /**
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function getQueryBuilder()
    {
        return $this->dbConnection->getConnection()->createQueryBuilder();
    }

    public function getCycles()
    {
        $query = $this->getQueryBuilder();
        $sth = $query
            ->select('*')
            ->from('cycles')
            ->execute();
        $data = $sth->fetchAll(\PDO::FETCH_ASSOC);

        $collection = new CycleCollection();



        foreach ($data as $crossroad) {

            $collection->add(CycleFactory::makeCycleFromArray($crossroad));
        }


        return $collection;

    }

}