<?php

namespace Sda\TrystarAPI\Cycle;


class CycleFactory
{
    public static function makeCycleFromArray(array $param)
    {

        return new Cycle($param['id'],$param['crossroads_id'],$param['name'],$param['active']);

    }
}