<?php

namespace Sda\TrystarAPI\Cycle;


use Sda\TrystarAPI\TypedCollection;

class CycleCollection extends TypedCollection
{

   public function __construct()
    {
        $this->setItemType(Cycle::class);

    }

}